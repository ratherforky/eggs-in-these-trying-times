module Main exposing (..)

import Playground exposing (..)
import List exposing (..)
import Angle exposing (radians, inDegrees)
import Tuple exposing (..)

-- MAIN

g = 9.81

type EggShape = Circle Float
              | Oval Float Float

type alias Egg  =
  { pos         : Pos
  , vx          : Float
  , vy          : Float
  , m           : Float
  , eggShape    : EggShape
  , cracked     : Bool
  }

type alias State =
  { egg        : Egg
  -- , background : Background,
  , foreground : List Object
  , pan        : Pan
  , dt         : Float
  , screen     : Screen
  }

type alias Pos = (Float, Float)

type Object = OShelf Shelf

type alias Shelf =
  { colour : Color
  , pos    : Pos
  , width  : Float
  , height : Float
  , angle  : Float
  }

type alias Pan =
  { pos    : Pos
  , bottom : Shelf
  , left   : Shelf
  , right  : Shelf
  , inside : Shelf
  }

type alias Screen =
  { rwidth  : Float -- Real width
  , rheight : Float -- Real height
  , vwidth  : Float -- Virtual width
  , vheight : Float -- Virtual height
  }

defaultEgg : Egg
defaultEgg = {pos = (370,270), vx = 0, vy = 0, m = 0.056, eggShape = (Circle 15), cracked=False }

main =
  game view update
    { egg = defaultEgg
    , foreground = [
        OShelf { colour = brown
        , pos = (370,150)
        , width = 800
        , height = 20
        , angle = pi/14
        }
      ]
    , pan = mkPan (-330, -240) 100 80 16
    , dt = 1/30
    , screen = { rwidth = 0, rheight = 0, vwidth = 800, vheight = 600 }
    }

mkPan : (Float, Float) -> Float -> Float -> Float -> Pan
mkPan pos w h t = --width height thickness
  let
    c = silver
  in
  { pos = pos
  , bottom = Shelf c (0,(t - h)/2) w t 0
  , left   = Shelf c ((t - w)/2,0) h t (pi/2)
  , right  = Shelf c ((w - t)/2,0) h t (pi/2)
  , inside = Shelf c (t/2,0) (w-2*t) (h-t) 0
  }

-- VIEW

-- Egg colour RBG constant
eggColour : Color
eggColour = rgb 255 245 195

silver : Color
silver = rgb 192 192 192

-- Funcation to translate eggShape to Playground Shape
drawEgg : Egg -> Shape
drawEgg egg  =
  let
    (x, y) = egg.pos
  in
  case egg.eggShape of
    Circle r ->
      circle eggColour r
      |> move x y
    Oval major minor ->
      oval eggColour major minor
      |> move x y

drawObject : Object -> Shape
drawObject (OShelf shelf) = drawShelf shelf

drawShelf : Shelf -> Shape
drawShelf shelf =
  rectangle shelf.colour shelf.width shelf.height
    |> movePos shelf.pos
    |> rotate (toDegrees shelf.angle)

movePos : Pos -> Shape -> Shape
movePos (x,y) = move x y

toDegrees : Float -> Float
toDegrees = Angle.radians >> inDegrees

drawPan : Pan -> Shape
drawPan pan = map drawShelf [
    pan.bottom, pan.left, pan.right
  ] |> group
    |> movePos pan.pos

scaleSizeInvariant : Screen -> Shape -> Shape
scaleSizeInvariant screen shape = 
  let
    ratio = screen.rwidth / screen.vwidth
  in
  scale ratio shape

view : Computer -> State -> List Shape
view computer c =
  [ rectangle (rgb 0 70 70 ) c.screen.vwidth c.screen.vheight
  , group (map drawObject c.foreground)
  , drawPan c.pan
  , drawEgg c.egg
  ] |> group
    >> scaleSizeInvariant c.screen
    |> singleton

-- UPDATE

detectCollision : Egg -> Shelf -> Bool
detectCollision egg shelf =
  let
    (x,y)   = egg.pos
    (x2,y2) = shelf.pos
    theta = shelf.angle
    plankHeight = shelf.height
    in
    case egg.eggShape of
      Circle r ->
        (sqrt((x-x2)^2 + (y-y2)^2)) <= ((cos theta)*(r + plankHeight))
      Oval major minor ->
        False

-- collides : Egg -> Shelf -> Bool
-- collides egg shelf = 

-- pointInShelf : Pos -> Shelf -> Bool
-- pointInShelf (x,y) shelf =
--   let
    

-- impact : Egg -> List Shelf -> Bool
perfectElasticCollision : Shelf -> Egg -> Egg
perfectElasticCollision shelf egg =
  let
    theta = atan2 egg.vy egg.vx - shelf.angle
    (vx, vy) = rotateV2 (2*pi - 2*theta) (egg.vx, egg.vy)
  in
  { egg | vx = vx, vy = vy }

rotateV2 : Float -> (Float, Float) -> (Float, Float)
rotateV2 theta (x, y) = (x*cos theta - y * sin theta, x*sin theta + y*cos theta)

getShelf : Object -> Shelf
getShelf (OShelf shelf) = shelf

collide : List Object -> Egg -> Egg
collide foreground egg = 
  map getShelf foreground
    |> filter (detectCollision egg)
    |> foldl perfectElasticCollision egg

gravity : Float -> (Float, Float)
gravity mass = (0, -g * mass)

vForce1D : Float -> Float -> Float -> Float -> Float
vForce1D u force mass dt = u + (force/mass) * dt

calcVelocityFromForce : Float -> (Float, Float) -> Egg -> Egg
calcVelocityFromForce dt (fx, fy) egg = { egg |
    vx = vForce1D egg.vx fx egg.m dt
  , vy = vForce1D egg.vy fy egg.m dt
  }

calcNextPos : Float -> Egg -> Egg
calcNextPos dt egg =
  let
    (x,y) = egg.pos
  in
  { egg | pos = (x + dt*egg.vx, y + dt*egg.vy) }

updateScreen computer screen =
  let
    w = computer.screen.width
    h = computer.screen.height
    b = computer.screen.bottom

    w2 = if w < 4*h/3 then w       else h*4/3
    h2 = if w < 4*h/3 then w*3/4   else h
  in
  { screen | rwidth  = w2
  , rheight = h2
  }

update computer c =
  let
    force = gravity c.egg.m
  in
  { c | egg = collide c.foreground c.egg
                |> calcVelocityFromForce c.dt force
                |> calcNextPos c.dt
  , screen = updateScreen computer c.screen }
